

OVERVIEW

This module simply creates a block that will display a list of node links that 
have been authored by the currently logged in user. The list can be filtered by 
specific content types, and can also be configured to display links to unpublished nodes
as well.



INSTALLATION

Simply copy the entire folder of this module to your Drupal installation's modules/ or sites/all/modules/ 
directory. If running a multi-site installation, you may choose to put this folder in your sites/*sitename*/modules/ 
directory.

After copying the folder to the appropriate location, visit admin/build/modules/ and enable the module.



CONFIGURATION

You can configure this module by visiting admin/build/block/ and going to the block's configuration page.
From here, you can set what content types should be displayed, and whether or not to display unpublished nodes
as well. That's all there is to it.



THEMING

The node links are themed by the function: theme_userAuthoredContentBlock($content). It is basically just a 
wrapper for theme('item-list'). Feel free to experiment with the output by overriding the theme function in your
theme's template.php file.

